package com.kenfogel.unit_test_02_demo;

import java.util.Arrays;
import java.util.Collection;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Test;

import static org.junit.Assert.*;

import com.kenfogel.business.ISPBusiness;
import com.kenfogel.business.ISPBusinessImpl;
import com.kenfogel.data.ISPBean;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A parameterized test class. Requires JUnit 4.11
 *
 * @author Ken
 */
@RunWith(Parameterized.class)
public class ISPBusinessTest {

    private final static Logger LOG = LoggerFactory.getLogger(ISPBusinessTest.class);

    /**
     * A static method is required to hold all the data to be tested and the
     * expected results for each test. This data must be stored in a
     * two-dimension array. The 'name' attribute of Parameters is a JUnit 4.11
     * feature
     *
     * @return The list of arrays
     */
    @Parameters(name = "{index} plan[{0}]={1}]")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {new ISPBean('A', 30), 49.95, 20, 40.0},
            {new ISPBean('A', 40), 69.95, 30, 60.0},
            {new ISPBean('B', 30), 24.95, 10, 10.0},
            {new ISPBean('B', 40), 34.95, 20, 20.0},
            {new ISPBean('C', 30), 19.95, 0, 0},
            {new ISPBean('C', 40), 19.95, 0, 0}
        });
    }
    // A Rule is implemented as a class with methods that are associared
    // with the lifecycle of a unit test. These methods run when required.
    // Avoids the need to cut and paste code into every test method.
    @Rule
    public MethodLogger methodLogger = new MethodLogger();

    private final ISPBean ispBean;
    private final double expectedTotal;
    private final double expectedAdditionlHours;
    private final double expectedAdditionalhoursCharge;

    /**
     * Constructor that receives all the data for each test as defined by a row
     * in the list of parameters
     *
     * @param ispBean
     * @param expectedTotal
     * @param expectedAdditionlHours
     * @param expectedAdditionalhoursCharge
     */
    public ISPBusinessTest(ISPBean ispBean, double expectedTotal, double expectedAdditionlHours, double expectedAdditionalhoursCharge) {
        this.ispBean = ispBean;
        this.expectedTotal = expectedTotal;
        this.expectedAdditionlHours = expectedAdditionlHours;
        this.expectedAdditionalhoursCharge = expectedAdditionalhoursCharge;
    }

    /**
     * Test of getAdditionalHours method, of class ISPBusiness.
     */
    @Test
    public void testGetAdditionalHours() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedAdditionlHours, instance.getAdditionalHours(), 0.01);
    }

    /**
     * Test of getAdditionalHoursCharge method, of class ISPBusiness.
     */
    @Test
    public void testGetAdditionalHoursCharge() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedAdditionalhoursCharge, instance.getAdditionalHoursCharge(), 0.01);
    }

    /**
     * Test of getTotalCost method, of class ISPBusiness.
     */
    @Test
    public void testGetTotalCost() {
        ISPBusiness instance = new ISPBusinessImpl(ispBean);
        instance.updateISPBean();
        assertEquals(expectedTotal, instance.getTotalCost(), 0.001);
    }

}
