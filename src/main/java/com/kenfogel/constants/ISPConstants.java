package com.kenfogel.constants;

/**
 * This class demonstrates how constants could be shared amongst multiple
 * classes. Many books and web sites use an Interface class to hold constants.
 * DO NOT DO this. There is no best way to share constants but if you must then
 * the static import from a class that cannot be instantiated (private
 * constructor). Oracle recommends doing this sparingly.
 *
 * References: http://en.wikipedia.org/wiki/Constant_interface
 * http://docs.oracle.com/javase/1.5.0/docs/guide/language/static-import.html
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public class ISPConstants {

    private ISPConstants() {
        // This class cannot be instantiated
    }
    public static final double PLANRATE_A = 9.95;
    public static final double PLANRATE_B = 14.95;
    public static final double PLANRATE_C = 19.95;
    public static final double ADDCOST_A = 2.0;
    public static final double ADDCOST_B = 1.0;
    public static final int INCLUDEDHOURS_A = 10;
    public static final int INCLUDEDHOURS_B = 20;
}
