package com.kenfogel.data;

/**
 * The java bean for the billing information. Avoid placing logic in data beans
 *
 * @author Ken Fogel
 * @version 1.0
 *
 */
public class ISPBean {

    private char planType;
    private double planCost;
    private int totalHours;
    private double hourlyCost;
    private int includedHours;

    public ISPBean() {
        super();
    }

    public ISPBean(char planType, int totalHours) {
        this.planType = planType;
        this.totalHours = totalHours;
    }

    public char getPlanType() {
        return planType;
    }

    public void setPlanType(char planType) {
        this.planType = planType;
    }

    public double getPlanCost() {
        return planCost;
    }

    public void setPlanCost(double planCost) {
        this.planCost = planCost;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public double getHourlyCost() {
        return hourlyCost;
    }

    public void setHourlyCost(double hourlyCost) {
        this.hourlyCost = hourlyCost;
    }

    public int getIncludedHours() {
        return includedHours;
    }

    public void setIncludedHours(int includedHours) {
        this.includedHours = includedHours;
    }

    @Override
    public String toString() {
        return "ISPBean [planType=" + planType + ", planCost=" + planCost
                + ", totalHours=" + totalHours + ", hourlyCost=" + hourlyCost
                + ", includedHours=" + includedHours + "]";
    }
}
