package com.kenfogel.business;

/**
 * Interface for the business class
 *
 * @author Ken Fogel
 * @version 1.0
 */
public interface ISPBusiness {

    public void updateISPBean();

    public double getAdditionalHours();

    public double getAdditionalHoursCharge();

    public double getTotalCost();
}
